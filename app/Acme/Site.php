<?php
use App\User;
/*

untuk keperluan multilanguange

*/
class Site
{
  /*
    parameter multilanguange di ambil dari session
    klo session gak ada di ambil dari config default languange
    setelah buat class ini
    daftar kan folder ini di composer.json
    kemudian dump autoload
    lalu buat file Locale di middleware
    lalu daftar kan class tersebut di kernel.php di $middlewareGroups

  */
  public static function langOption()
   {
       $path = base_path().'/resources/lang/';
       $lang = scandir($path);

       $t = array();
       foreach($lang as $value) {
           if($value === '.' || $value === '..') {continue;}
           if(is_dir($path . $value))
           {
               $fp = file_get_contents($path . $value.'/info.json');
               $fp = json_decode($fp,true);
               $t[] =  $fp ;
           }

       }
       return $t;
   }


    public static function Profile()
    {
        $profile=User::find('1');

        return $profile;
    }
}
