<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class Post extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    use Sluggable;

    protected $table = 'posts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'desc', 'content', 'published_at', 'series_id', 'users_id','status'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /*
      untuk set value users_id
    */

    public function setStatusAttribute($value)
    {
      return  $this->attributes['status'] = ($value=='1') ? '1':'0';

    }

    /*
    *
    * untuk set value date pada form create
    */
    public function getPublishedatAttribute($date)//untuk set format pada form variable post lo g
    {
        return new Carbon($date);
    }

    /*
      relasi dengan table post tags untuk proses input data baru
    */

    public function CreateInputTag()
    {
      return $this->belongsToMany('App\Tag','post_tags','posts_id','tags_id')->withTimestamps();
    }


    /*
      untuk relasi ke post hit
      agar input data setiap user yg view untuk menghintung berapa kali
      user yg hit sebuah artikel
    */
    public function CreatePostView()
    {
        return $this->belongsToMany('App\PostHit','post_hits','post_id')->withTimestamps();
        #return $this->belongsTo('App\PostHit','post_hits','post_id');

    }

    public function getTagAttribute()//untuk set select pada fungsi edit
    {
      return $this->CreateInputTag->pluck('id')->all();
    }

    /*
        untuk meanmpilkan penulis post
    */
    public function PostByWriter()
    {
      return $this->belongsTo('App\User','users_id');
    }



    public function scopePublished($query) //buat filter artikel yang akan di publish
    {
        return $query->where('published_at','<=',Carbon::now())
                     ->where('status','=','1')
                     ->orderBy('id','desc');

    }
}
