<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\Tag;
use App\PostTag;
use App\Series;
use Illuminate\Http\Request;
use Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $post = Post::paginate(25);

        return view('admin.post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $series=Series::pluck('name','id');
        $tag=Tag::pluck('name','id');
        return view('admin.post.create',compact('series','tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
    			'title' => 'required',
    			'desc' => 'required',
    			'content' => 'required'
    		]);
        $requestData = $request->all();
        #dd($requestData);

        $post = new Post($requestData);
        $post->users_id= Auth::user()->id;
        $post->save();

        $post->CreateInputTag()->attach($request->input('tag'));
        flash('Post added!');

        return redirect('admin/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('admin.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $series=Series::pluck('name','id');
        $tag=Tag::pluck('name','id');
        return view('admin.post.edit', compact('post','series','tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			       'title' => 'required',
			          'desc' => 'required',
			             'content' => 'required'
		               ]);
        $requestData = $request->all();

        $post = Post::findOrFail($id);
        $post->slug=null;
        $post->status=isset($request['status']);
        $post->users_id= Auth::user()->id;
        $post->update($requestData);
        $post->CreateInputTag()->sync($request->input('tag'));
        flash('Post updated!');

        return redirect('admin/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Post::destroy($id);

        flash('Post deleted!');

        return redirect('admin/post');
    }
}
