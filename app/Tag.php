<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;
class Tag extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    use Sluggable;
    protected $table = 'tags';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    public function sluggable()
   {
       return [
           'slug' => [
               'source' => 'name'
           ]
       ];
   }

   public function PostArticle()
   {
      return $this->belongsToMany('App\Post','post_tags','tags_id','posts_id');
   }


}
