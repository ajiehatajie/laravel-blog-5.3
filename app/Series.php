<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;

class Series extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    use Sluggable;
    protected $table = 'series';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'desc'];

    public function sluggable()
   {
       return [
           'slug' => [
               'source' => 'name'
           ]
       ];
   }

   public function article()
   {
     return $this->hasMany('App\Post');
   }

}
