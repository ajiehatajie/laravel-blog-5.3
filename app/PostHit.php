<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostHit extends Model
{

        protected $table = 'post_hits';

        protected $primaryKey = 'id';
}
