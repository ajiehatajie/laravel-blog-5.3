@extends('layouts.blog')

@section('content')
<div class="col-md-10 col-lg-8">

<article>
<h1> {{Lang::get('core.series')}}</h1>

      <ul class="post-list">

      @unless(count($series)==0)
        @foreach ($series as $seri)
        <li  class="post-list__post" style="margin-bottom: 0.3em;">
        {!! link_to('blog/series/'.$seri->slug,
                        ucfirst($seri->name),
                         $attributes =
                         ['class'=>'post-list__link'], $secure = null) !!}
         </li>
        @endforeach
      @endunless

    </ul>

     <hr/>

      <h1> {{Lang::get('core.article')}}</h1>


      <ul class="post-list">
                        @unless(count($posts)==0)
                            @foreach ($posts as $post)
                              <li class="post-list__post" style="margin-bottom: 0.3em;">
                                {!!link_to('blog/'.$post->slug,
                                   ucfirst($post->title),
                                   $attributes = ['class'=>'list__link'],
                                    $secure = null) !!}
                                    <p class="post-list__preview">
                                      {!!
                                        ucfirst(str_replace('""', '',
                                         str_limit($post->desc,200))) !!}
                                    </p>
                              </li>
                            @endforeach
                        @endunless
      </ul>

                      {!! $posts->links() !!}
</article>
</div>
@include('layouts.blogsidebar')
@endsection
