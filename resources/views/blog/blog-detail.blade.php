@extends('layouts.blog')

@section('content')

<div class="col-md-10 col-lg-8">
    <article>
    <h1>{!! ucfirst($post->title) !!}
       <small></small></h1>
        <h5 class="article-metadata">{{ $post->published_at->format('M jS Y g:ia') }}
           | total view:
           {{
           count($posthit)
           }} | By <strong> {{ucfirst($post->PostByWriter->name)}}</strong></h5>
           <hr/>
    <div id="article">
        {!! ucfirst($post->content) !!}
    </div>

    <hr/>
    @unless(count($post->CreateInputTag)==0)

    Tag :
    @foreach ($post->CreateInputTag as $key)

    {!! link_to('tags/'.$key->slug
               ,ucfirst($key->name),['class'=>'list__link'],null)  !!}
    {{$loop->remaining ? ',':''}}
   @endforeach

    @endunless


  </article>
</div>
@include('layouts.blogsidebar')
@endsection
