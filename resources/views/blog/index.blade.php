@extends('layouts.blog')

@section('content')

 <div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1">

        <div class="homepage-body">

            <img src="/img/profile/{{Site::Profile()->photo}}"
            alt="ajie hatajie" class="homepage-headshot">


            <p class="intro">
                {!! str_replace('""', '', Site::Profile()->qoutes) !!}
            </p>
        </div>

  </div>

@endsection
