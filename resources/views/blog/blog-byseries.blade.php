@extends('layouts.blog')
@section('content')
  <div class="col-md-10 col-lg-8">
    <style>
    ul {
    display: block;
    list-style-type: decimal;
    -webkit-margin-before: 1em;
    -webkit-margin-after: 1em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    -webkit-padding-start: 40px;
      }
    </style>
    <article>
        <h1>{!! ucfirst($filter->name) !!}</h1>
        <p>
          {!! ucfirst($filter->desc) !!}
        </p>
        <br/>
        <ul>

        @foreach ($filter->article as $key)
        <li>

            @if($key->status=='1')

                  @if($key->published_at <= Carbon\Carbon::now())

                    {!! link_to('/blog'.'/'.$key->slug,ucfirst($key->title), ['class'=>'post-list__link'], false) !!}

                  @else

                    {{ucfirst($key->title)}} (Comming Soon)

                  @endif
            @else

              {{$key->title}}

            @endif
            </li>
        @endforeach
      </ul>
  </article>
    </div>
@include('layouts.blogsidebar')
@endsection
