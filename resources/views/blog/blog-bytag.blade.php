@extends('layouts.blog')

@section('content')
  <div class="col-md-10 col-lg-8">

  @unless (count($post)==0)

    <h1>Filter Post by Tag: "{{ ucfirst(Request::segment(2))}}"</h1>
  @foreach ($post as $key)

  <ul class="post-list">
      <li>
      <span>  {{ $key->published_at->diffForHumans() }} </span>
         -
      {!!
      link_to('/blog/'.$key->slug,ucfirst($key->title),['class'=>'list__link'],null)
      !!}
      </li>
  </ul>
  @endforeach
  @endunless

    </div>
@include('layouts.blogsidebar')
@endsection
