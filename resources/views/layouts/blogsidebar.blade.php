
<div class="col-lg-3 col-lg-push-1 col-md-2 right-rail">
  <div class="rail-bio">
						<img src="/img/profile/{{Site::Profile()->photo}}"
            alt="ajiehatajie" class="rail-bio__headshot">
						<p>
              {!! str_replace('""', '', Site::Profile()->qoutes) !!}
            </p>

            <p style="margin-top: 1.5em;">
              <a href="https://twitter.com/ajiehatajie"
              class="twitter-follow-button"
              data-show-count="true" data-size="medium" data-dnt="true">Follow @ajiehatajie</a>
            <script>!function(d,s,id){
              var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></p>
 </div>
 <hr/>

</div>
