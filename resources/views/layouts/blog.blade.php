<!DOCTYPE html>
<html lang="{{Lang::get('core.flag')}}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Blog personal Ajie Hatajie. berisi catatan,tutorial PHP dan Pemprograman Lain serta Pengalaman Pribadi">
        <meta name="keywords" content="Catatan HTML,CSS,JavaScript,PHP,Blog ajie hatajie">
        <meta name="author" content="Ajie Hatajie">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}|Catatan dan Tutorial PHP dan Pemprograman Basis Web </title>

        <!-- Styles -->
        <link href="/css/bootstrap-cosmo.css" rel="stylesheet">
        <link href="/css/main.css" rel="stylesheet">
        <link href="/prism.css" rel="stylesheet">

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/blog') }}">
                        Blog <span class="sr-only">(current)</span></a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">


                    // Check role anywhere
                    @if(Auth::check() && Auth::user()->hasRole('superadmin'))
                        <li><a href="{{ url('/admin') }}">{{Lang::get('core.adminpanel')}}</a></li>

                    @else
                        // Do nothing
                    @endif
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">{{Lang::get('core.login')}}</a></li>
                            <li><a href="{{ url('/register') }}">{{Lang::get('core.register')}}</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{Lang::get('core.logout')}}
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        <!--
                        start multilanguange
                        -->
                        <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  {{ Lang::get('core.languange')}}

                                  <img class="flag-lang"
                                  src="/img/{{Lang::get('core.flag')}}.png">

                                   <span class="caret"></span>
                              </a>
                          <ul class="dropdown-menu" role="menu">
                            @foreach(Site::langOption() as $lang)
                                    <li>
                                    <a href="{{ URL::to('blog/lang/'.$lang['folder'])}}"><img class="flag-lang" src="/img/{{$lang['folder']}}.png"> {{  $lang['name'] }}
                                    </a>
                                    </li>
                           @endforeach
                           </ul>
                       </li>
                          <!--
                          end multilanguange
                          -->

                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
             @include('flash::message')

             <div class="row">

             @yield('content')

             </div>
        </div>


        <hr/>

        <footer class="footer">
             <div class="container">
                 <p>
                 &copy; 2016 {{config('app.name')}} &nbsp;&bull;&nbsp; <a href="http://twitter.com/ajiehatajie">@ {{config('app.name')}}</a> &nbsp;&bull;&nbsp; <a href="/blog/feed.atom">RSS</a> &nbsp;&bull;&nbsp; Like what I write? Hire <a href="http://benang.co.id/?utm_source=hatajie.com&utm_medium=footer">Bcomm</a> and we can work together
                 </p>
         </div>
         </footer>

        <!-- Scripts -->

        <!-- This is only necessary if you do Flash::overlay('...') -->
        <script src="/js/jquery.js"></script>
        <script src="/prism.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        @yield('footer')
        <script>
            $('#flash-overlay-modal').modal();
        </script>
        <script type="text/javascript">
            $(function ()
             {
                // Navigation active
                $('ul.navbar-nav a[href="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"]').closest('li').addClass('active');
            });
        </script>
        <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        </script>

    </body>
</html>
