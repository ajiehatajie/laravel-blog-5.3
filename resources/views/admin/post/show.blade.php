@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Post {{ $post->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('admin/post/' . $post->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Post"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/post', $post->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Post',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $post->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $post->title }} </td></tr>
                                    <tr><th> Slug </th><td> {{ $post->slug }} </td></tr>
                                    <tr><th> Desc </th><td> {{ $post->desc }} </td></tr>
                                    <tr><th> Tag </th>
                                      <td>

                                        @unless(count($post->CreateInputTag)==0)
                                            <ul>
                                            @foreach ($post->CreateInputTag as $tags)
                                                <li>{{$tags->name}}</li>
                                            @endforeach
                                            </ul>
                                        @endunless


                                      </td>
                                    </tr>
                                    <tr><th> Published At</th><td> {{ $post->published_at->diffForHumans() }} </td></tr>
                                    <tr><th> Status </th>
                                      <td>
                                        {{$post->status=='0'?'Unpublished':'Publish'}}
                                    </td>
                                  </tr>


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
