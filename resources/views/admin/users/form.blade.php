<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-11">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Email: ', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-11">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password', 'Password: ', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-11">
        {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('Picture', 'Picture: ', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-11">
        {!! Form::file('photo', ['class' => 'form-control']) !!}
        {!! $errors->first('Picture', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('qoutes') ? ' has-error' : ''}}">
    {!! Form::label('qoutes', 'qoute: ', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-11">
        {!! Form::textarea('qoutes',null,['class'=>'ckeditor']) !!}
        {!! $errors->first('qoutes', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
    {!! Form::label('role', 'Role: ', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-11">
        <select class="roles form-control" id="roles" name="roles[]" multiple="multiple">
            @foreach($roles as $role)
            <option value="{{ $role->name }}">{{ $role->label }}</option>
            @endforeach()
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-11 col-md-1">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@section('footer')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'ckeditor' );
</script>
@endsection
