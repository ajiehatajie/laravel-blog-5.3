<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-11">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
    {!! Form::label('desc', 'Desc', ['class' => 'col-md-1 control-label body','id'=>'body']) !!}
    <div class="col-md-11">
        {!! Form::textarea('desc', null, ['class' => 'form-control ckeditor', 'required' => 'required']) !!}
        {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-1 col-md-1">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>


@section('footer')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

 <script>

      CKEDITOR.replace( 'ckeditor' );
  </script>
@endsection
