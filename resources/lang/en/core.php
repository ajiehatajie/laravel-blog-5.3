<?php
return
array(
    "norecord"		  	=> "Data Not Found",
    "create"			    => "Create New",
    "edit"			      => "Edit",
    "delete"			    => "Delete",
    "view"			      => "Show",
    "detail"			    => "Detail",
    "flag"            => "en",
    "languange"       => "Languange",
    "article"         => "Article",
    "series"          => "Series",
    "home"            => "Home",
    "logout"          => "Logout",
    "login"           => "Login",
    "register"        => "Register",
    "back"            => "Back",
    "adminpanel"      =>"Admin Panel",


   );
