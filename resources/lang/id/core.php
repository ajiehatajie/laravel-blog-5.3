<?php
return
array(
    "norecord"			=> "Tidak Ada di temukan data",
    "create"		  	=> "Buat Baru",
    "edit"			    => "Ubah",
    "delete"			  => "Hapus",
    "view"			    => "Lihat",
    "detail"			  => "Detail",
    "flag"          => "id",
    "languange"     => "Bahasa",
    "article"       => "Artikel",
    "series"        => "Seri",
    "home"          => "Beranda",
    "logout"        => "Keluar",
    "login"         => "Masuk",
    "register"      => "Daftar",
    "back"          => "Back",
    "adminpanel"    =>"Admin Panel",
   );
