<?php
Auth::routes();
Route::get('/','BlogController@index');
Route::get('/blog','BlogController@blog');
Route::get('/blog/{slug}','BlogController@blogDetail')->where('slug', '[A-Za-z-1-9]+');
Route::get('/tags/{tag}','BlogController@tag')->where('tag', '[A-Za-z-1-9]+');
Route::get('/blog/series/{series}','BlogController@Series')->where('series', '[A-Za-z-1-9]+');
Route::get('/blog/lang/{id}', 'BlogController@getLang')->where('id','[a-z]+');
Route::get('/home', 'HomeController@index');


Route::group(['namespace'=>'Admin','middleware'=>['auth','roles'], 'roles' => 'superadmin','group'=>'Admin'],
    function()
    {
      Route::get('admin', 'AdminController@index');
      Route::get('admin/give-role-permissions', 'AdminController@getGiveRolePermissions');
      Route::post('admin/give-role-permissions', 'AdminController@postGiveRolePermissions');
      Route::resource('admin/roles', 'RolesController');
      Route::resource('admin/permissions', 'PermissionsController');
      Route::resource('admin/users', 'UsersController');
      Route::resource('admin/series-category', 'SeriesController');
      Route::resource('admin/tagging', 'TagController');
      Route::resource('admin/post', 'PostController');
      Route::resource('admin/subscriber', 'SubscriberController');
      Route::get('admin/subs','SubscriberController@getform');
      Route::get('admin/visitor','VisitController@index');
      Route::get('/admin/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    #  Route::get('/admin/logs', 'VisitController@log');


      });
      Route::get('hi', function()
      {
          echo 'hi, saya web artisan';
      });



use Illuminate\Support\Facades\Input;
Route::get('/test/{tes}', function($tes)
{
   dd($tes);

})->where('tes', '[A-Za-z-1-9]+');
